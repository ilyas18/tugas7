<?php 

require('./animal.php');

class Ape extends Animal {
    public $yell = "auoo";

    function setApe($yell){
        $this->yell = $yell;
    }
}