<?php 
require('./animal.php');
// require('./frog.php');
// require('./ape.php');


// objek baru dari kelas Animal


$sheep = new Animal();
$sheep->setAnimal("jerapah", "4", "no");
echo $sheep->name."<br>"; // shaun
echo $sheep->legs."<br>"; // 4
echo $sheep->cold_blooded."<br>"; //no

$kera = new Animal();
$kera->setAnimal("kera", "2", "no");
$kera->setFrog("hop hop");
echo $kera->name."<br>";
echo $kera->legs."<br>";
echo $kera->cold_blooded."<br>";
echo $kera->jump."<br>";
